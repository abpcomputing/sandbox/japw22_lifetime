# %% source /home/lumimod/2022_09_29_NXCALS/venv/bin/
from pathlib import Path
import os
import getpass
import logging
import sys
import nx2pd as nx 
import pandas as pd
import time
import shutil

import yaml

with open('config.yaml','r') as fid:
    configuration = yaml.safe_load(fid)

TZONE = configuration['time_zone']

# Where to store the parquet files
my_path = Path('/home/sterbini/japw22_lifetime/')

#from cron_config import datavars

#logging.basicConfig(stream=sys.stdout, level=logging.INFO)
#logging.basicConfig(stream='test.log', level=logging.INFO)
logging.basicConfig(filename=configuration['logfile_path'], encoding='utf-8',
                    level=logging.INFO,
                    format='%(message)s')
logging.info('\n'+40*'=')
logging.basicConfig(filename=configuration['logfile_path'], encoding='utf-8', 
                    level=logging.INFO,force=True,
                    format='%(asctime)s %(message)s')

os.environ['PYSPARK_PYTHON'] = "./environment/bin/python"
username = getpass.getuser()
print(f'Assuming that your kerberos keytab is in the home folder, ' 
      f'its name is "{getpass.getuser()}.keytab" '
      f'and that your kerberos login is "{username}".')

logging.info('Executing the kinit')
os.system(f'kinit -f -r 5d -kt {os.path.expanduser("~")}/{getpass.getuser()}.keytab {getpass.getuser()}');

# %%
logging.basicConfig(format='%(asctime)s %(message)s')

from nxcals.spark_session_builder import get_or_create, Flavor
logging.info('Creating the spark instance')
# Using LOCAL instead of YARN_LARGE
spark = get_or_create(flavor=Flavor.LOCAL,
conf={'spark.driver.maxResultSize': '8g', 
    'spark.executor.memory':'8g',
    'spark.driver.memory': '16g',
    'spark.executor.instances': '20',
    'spark.executor.cores': '2',
    })
sk  = nx.SparkIt(spark)
logging.info('Spark instance created.')


data = [
 'HX:FILLN',
 'HX:BMODE',
 'LHC.BSRT.5L4.B2:BUNCH_EMITTANCE_H',
 'LHC.BSRT.5L4.B2:BUNCH_EMITTANCE_V',
 'LHC.BSRT.5R4.B1:BUNCH_EMITTANCE_H',
 'LHC.BSRT.5R4.B1:BUNCH_EMITTANCE_V',
 ]

def get(t0, t1, data):
    my_var = sk.get_variables(data)
    my_df = []
    for ii in my_var:
        df = sk.nxcals_df([ii['name']], 
                    t0,
                    t1,
                    pandas_processing=[
                        nx.pandas_get, 
                        nx.pandas_pivot])
        for my_column, my_type in zip(df.columns,df.dtypes):
                if my_type==object:
                    if (len(df[my_column].dropna())>0) and (type(df[my_column].dropna().iloc[0])==dict):
                        df[my_column] = df[my_column].apply(lambda x:x['elements'] if pd.notnull(x) else x)
        my_df.append(df.sort_index())
    return pd.concat(my_df, axis=1).sort_index()

# 2022-11-22 07:40:07.315 - 2022-11-22 12:06:58.332
# Fill: 8460 (START OF FILL - END OF FILL)

# 2022-11-22 12:06:58.332 - 2022-11-22 15:29:23.916
# Fill: 8461 (START OF FILL - END OF FILL)

# 2022-11-22 15:29:23.916 - 2022-11-22 18:28:16.849
# Fill: 8462 (START OF FILL - END OF FILL)

# 2022-11-23 13:27:43.414 - 2022-11-23 15:51:15.840
# Fill: 8469 (START OF FILL - END OF FILL)

# 2022-11-23 15:51:15.840 - 2022-11-23 19:55:47.378
# Fill: 8470 (START OF FILL - END OF FILL)

# 2018-10-23 16:36:56.815 - 2018-10-24 06:02:22.752
# Fill: 7334 (STABLE - END OF FILL)


# 2022-11-26 23:58:05.788 - 2022-11-27 08:54:59.443
# Fill: 8491 (STABLE - STABLE)

myDict= {8460: {'t1':'2022-11-22 07:40:07.315',  't2':'2022-11-22 12:06:58.332'},
         8461: {'t1':'2022-11-22 12:06:58.332',  't2':'2022-11-22 15:29:23.916'},
         8462: {'t1':'2022-11-22 15:29:23.916',  't2':'2022-11-22 18:28:16.849'},
         8469: {'t1':'2022-11-23 13:27:43.414',  't2':'2022-11-23 15:51:15.840'},
         8470: {'t1':'2022-11-23 15:51:15.840',  't2':'2022-11-23 19:55:47.378'},
         7035: {'t1':'2018-08-07 17:51:30.783',  't2':'2018-08-07 18:37:32.928'},
         7334: {'t1':'2018-10-23 16:36:56.815',  't2':'2018-10-24 06:02:22.752'},
         8491: {'t1':'2022-11-26 23:58:05.788',  't2':'2022-11-27 08:54:59.443'},
         }
# %%
for my_fill in [8491]:
    logging.info(f'FILL {my_fill}')

    t0 = pd.Timestamp(myDict[my_fill]['t1'],tz=TZONE)
    t1 = pd.Timestamp(myDict[my_fill]['t2'],tz=TZONE)

    # Test mode:
    if False:
        t1 = pd.Timestamp.now(tz=TZONE) - pd.Timedelta(hours=10) 
        t0 = t1 - pd.Timedelta(hours=2)

    my_df = get( t0, t1, data)
    my_df['HX:FILLN']=my_fill
    my_df.to_parquet(my_path / f'{my_fill}_BSRT.parquet')


    logging.info(f'Data Saved,[{str(t0)[11:19]} - {str(t1)[11:19]}]'+'\n'+40*'=')

# %%

import glob
import os
if True:
    files = sorted(glob.glob(str(my_path / '*.parquet')))
    if len(files) > 5:
        for file in files[:-5]:
            os.remove(file)
# %%
